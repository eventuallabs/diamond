#ifndef VOLUME_H
#define VOLUME_H

// How many ADC counts before the system registers a volume knob change
#define VOLUME_NOISE_COUNTS 2

void volume_init(void);

// Read the volume knob position
uint16_t volume_read(void);

#endif
