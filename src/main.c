
#include <stdio.h>
#include <string.h>
#include <avr/interrupt.h>

// Watchdog timer
#include <avr/wdt.h>

// Convenience functions for busy-wait loops
#include <util/delay.h>

// Contains macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

// Provides the system state enumerated type
#include "system.h"

// Functions for setting the system clock prescaler
#include "clock.h"

// Contains the extern declaration of command_array -- an array
// containing all the commands understood by the system.
#include "command.h"

#include "usart.h"

// Log messages based on severity and subsystem
#include "logger.h"

// Functions dealing with periodic interrupts
#include "metronome.h"

// Functions for working with the SPI hardware
#include "spi.h"

// Functions for simple scheduling
#include "OS.h"

// Provides functions for writing to and reading from the eeprom.
#include "eeprom.h"

// Provides bbuart_puts()
#include "bbuart.h"

/* led.h

   Provides functions for turning the LED on and off.
*/
#include "led.h"

// Provides watchdog_init()
#include "watchdog.h"

// Provide support for i2c devices
#include "i2c.h"

// Provides support for audio amplifiers
#include "amplifier.h"

// Support for reading the volume knob via the ADC
#include "volume.h"

// Front panel
#include "adams.h"

/* Set the measurement array size.

   As things are now, this will also be the number of readings to
   average to create an output datum.  But this size can grow
   independent of the number of averages.
*/
#define MEASUREMENT_ARRAY_SIZE 4

/* This array will hold measurements made in the main loop.
 */
uint16_t measurement_array[MEASUREMENT_ARRAY_SIZE];

// Define a pointer to the received command state
recv_cmd_state_t  recv_cmd_state;
recv_cmd_state_t *recv_cmd_state_ptr = &recv_cmd_state;

int main(void) {

  sei(); // Enable interrupts

  // Set up the system clock.  Do this before setting up the USART,
  // as the USART depends on this for an accurate buad rate.
  fosc_16MHz();

  // watchdog_init();

  //  Set up the USART before setting up the logger -- the logger uses
  //  the USART for output.
  usart_init();
  bbuart_init();

  // Initialize the logger.  The default log level is set in the
  // makefile.  Note that nothing can be logged before this line.
  logger_init();

  /* To configure the logger, first clear the logger enable register
     by disabling it with logger_disable().  Then set individual bits
     with logger_setsystem().
  */
  logger_disable(); // Disable logging from all systems

  // Enable logger system logging
  logger_setsystem( "logger" );

  // Enable metronome logging
  logger_setsystem( "metronome" );

  logger_setsystem( "rxchar" ); // Enable received character logging
  // logger_setsystem( "command" ); // Enable command system logging
  logger_setsystem( "adc" ); // Enable adc module logging

  // Initialize the I2C bus
  i2c_init();

  logger_setsystem( "eeprom" ); // Enable eeprom module logging
  logger_setsystem( "cal" ); // Enable calibration module logging

  // logger_setsystem( "spi" );

  logger_setsystem( "main" );

  // The audio amplifier module
  logger_setsystem( "amplifier" );

  // The MAX7944 amplifier module
  // logger_setsystem( "max9744" );

  // The volume knob module
  // logger_setsystem( "volume" );

  // logger_setsystem( "adams" );

  // The system module
  logger_setsystem( "system" );

  led_init();

  // Start the metronome
  metronome_init();

  amplifier_init();

  volume_init();

  adams_init();

  // Initialize all the peripherals before initializing the system.
  // This init function call should be last.
  system_init();

  command_init( recv_cmd_state_ptr );

  logger_msg_p("main", log_level_INFO, PSTR("Firmware version is %s"),
	       REVCODE);

  // The system has now booted.  Cough up the identity string to help
  // with making serial connections and to measure boot time.
  system_print_identity();

  // The main loop
  for(;;) {

    // Process the parse buffer to look for commands loaded with the
    // received character ISR.
    command_process_pbuffer( recv_cmd_state_ptr, command_array );

    // Execute scheduled tasks
    OS_TaskExecution();

    // Reset the watchdog
    wdt_reset();

  }// end main for loop

} // end main

//*************************** Interrupts ***************************//

// Find the name of interrupt signals in iom328p.h.

// See the naming convention outlined at
// http://www.nongnu.org/avr-libc/user-manual/group__avr__interrupts.html
// to make sure you don't use depricated names.

// Interrupt on character received via the USART
ISR(USART_RX_vect) {
  // Write the received character to the buffer
  *(recv_cmd_state_ptr -> rbuffer_write_ptr) = UDR0;
  if (*(recv_cmd_state_ptr -> rbuffer_write_ptr) == '\r') {
    logger_msg_p("rxchar",log_level_ISR,
		 PSTR("Received a command terminator"));
    if ((recv_cmd_state_ptr -> rbuffer_count) == 0) {
      // We got a terminator, but the received character buffer is
      // empty.  The user is trying to clear the transmit and
      // receive queues.
      return;
    }
    else {
      if ((recv_cmd_state_ptr -> pbuffer_locked) == true) {
	// We got a terminator, and there are characters in the
	// received character buffer, but the parse buffer is locked.
	// This is bad -- we're receiving commands faster than we can
	// process them.
	logger_msg_p("rxchar",log_level_ERROR,
		     PSTR("Command process speed error!"));
	rbuffer_erase(recv_cmd_state_ptr);
	return;
      }
      else {
	// We got a terminator, and there are characters in the
	// received character buffer.  The parse buffer is
	// unlocked so terminate the received string and copy it
	// to the parse buffer.
	*(recv_cmd_state_ptr -> rbuffer_write_ptr) = '\0';
	strcpy((recv_cmd_state_ptr -> pbuffer),
	       (recv_cmd_state_ptr -> rbuffer));
	recv_cmd_state_ptr -> pbuffer_locked = true;
	logger_msg_p("rxchar",log_level_ISR,
		     PSTR("Parse buffer contains '%s'"),
		     (recv_cmd_state_ptr -> pbuffer));
	rbuffer_erase(recv_cmd_state_ptr);
	return;
      }
    }
  }
  else {
    // The character is not a command terminator.
    (recv_cmd_state_ptr -> rbuffer_count)++;
    logger_msg_p("rxchar",log_level_ISR,
		 PSTR("%c  <-- copied to receive buffer.  Received count is %d"),
		 *(recv_cmd_state_ptr -> rbuffer_write_ptr),
		 recv_cmd_state_ptr -> rbuffer_count);
    if ((recv_cmd_state_ptr -> rbuffer_count) >= (RECEIVE_BUFFER_SIZE-1)) {
      logger_msg_p("rxchar",log_level_ERROR,
		   PSTR("Received character number above limit"));
      command_nack(NACK_BUFFER_OVERFLOW);
      rbuffer_erase(recv_cmd_state_ptr);
      return;
    }
    else {
      // Increment the write pointer
      (recv_cmd_state_ptr -> rbuffer_write_ptr)++;
    }
  }
  return;
}

