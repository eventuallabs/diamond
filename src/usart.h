
/* stdint.h
 * Defines fixed-width integer types like uint8_t
 */
#include <stdint.h>


// Set baud rate
// |--------+-------+----+--------+--------|
// |   Baud | fosc  | 2x | UBRR0H | UBRR0L |
// |--------+-------+----+--------+--------|
// |   9600 | 16MHz |  0 |      0 |    103 |
// |  38400 | 16MHz |  0 |      0 |     25 |
// | 115200 | 16MHz |  0 |      0 |      8 |
// | 115200 | 16MHz |  1 |      0 |     16 |
// |--------+-------+----+--------+--------|
#define USART_UBRR 25

// Set double or single speed
#define USART_U2X 0


// Line termination characters
#define LINE_TERMINATION_CHARACTERS "\n"

// The maximum string length that can be sent as output over the
// command  or debug interfaces
#define PRINT_BUFFER_SIZE 80

// Declare a buffer for printing log and remote interface output over
// USB.  Make this extern so that we can use it in any source file
// that includes this file.
//
// Making this statically allocated makes the RAM footprint more
// predictable.
extern char PRINT_BUFFER[];

// Send a formatted string to the USART
//
// Arguments:
//   *fmt -- Format string
//   ... -- Data for the format string
uint8_t usart_printf ( char *fmt, ... );

// Send a formatted string stored in flash memory to the USART
//
// Arguments:
//   *fmt -- Format string
//   ... -- Data for the format string
uint8_t usart_printf_p(const char *fmt, ...);

// Simple USART receive function based on polling of the receive
// complete (RXCn) flag.
unsigned char usart_receive(void);

// Send a character to the USART
//
// Arguments:
//   data -- Character to send
void usart_putc(char data);

// Send a string to the USART
//
// Arguemnts:
//   s[] -- String to send
void usart_puts(char s[]);

// Send a string stored in flash memory to the USART
//
// Arguments:
//   *data -- String to send
void usart_puts_p( const char *data );

/* usart_init() 

   Initialize the USART for 9600 baud, 8 data bits, 1
   stop bit, no parity checking.
*/
void usart_init(void);

/* usart_76k8_baud()

   Set the USART's baud rate to 76.8k baud.  This is a strange baud,
   and it won't be available with slower system clocks.  Call
   usart_init() before using this.
*/
void usart_76k8_baud(void);
