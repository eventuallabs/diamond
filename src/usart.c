
#include <stdio.h>

// Provides strcmp
#include <string.h>

// Allows functions to accept an indefinite number of arguments.
#include <stdarg.h>

// Device-specific port definitions
#include <avr/io.h>

// Contains macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>
#include "usart.h"

// Any file that includes usart.h will have access to PRINT_BUFFER
char PRINT_BUFFER[PRINT_BUFFER_SIZE];

// Send a formatted string to the USART
uint8_t usart_printf ( char *fmt, ... ) {
    va_list args;

    va_start (args, fmt);

    /* For this to work, printbuffer must be larger than
    * anything we ever want to print.
    */
    vsnprintf (PRINT_BUFFER, PRINT_BUFFER_SIZE, fmt, args);
    va_end (args);

    // Print the string
    usart_puts( PRINT_BUFFER );
    return 0;
}

// Send a format string stored in flash memory to the USART.
uint8_t usart_printf_p( const char *fmt, ... ) {
  va_list args;

  va_start ( args, fmt );
  
  // To avoid truncating output, PRINT_BUFFER_SIZE must be larger than
  // anything we ever want to print.
  vsnprintf_P (PRINT_BUFFER , PRINT_BUFFER_SIZE, fmt, args);

  va_end (args);

  // Print the buffer
  usart_puts( PRINT_BUFFER );
  return 0;
}

unsigned char usart_receive(void) {
    while( !(UCSR0A & (1<<RXC0)));
    return UDR0;
}

// Send a character to the USART
void usart_putc(char data) {
  // Wait for empty transmit buffer
  loop_until_bit_is_set(UCSR0A,UDRE0);
  // Put data into buffer -- sends the data
  UDR0 = data;
}

// Sends a string over the USART by repeatedly calling usart_putc()
void usart_puts(char *data_ptr) {
  uint8_t i = 0;
  while(i < PRINT_BUFFER_SIZE) // don't get stuck if it is a bad string
    {
      if( data_ptr[i] == '\0' ) break; // quit on string terminator
      usart_putc(data_ptr[i++]);
    }
}

// Send string stored in flash memory to the USART
void usart_puts_p(const char *data_ptr) {
    uint8_t txdata = 1; // Dummy initialization value
    while ( txdata != 0x00 ) {
        txdata = pgm_read_byte(data_ptr);
        usart_putc(txdata);
        data_ptr++;
    }
}

void usart_init( void ) {
  // Initialize the USART.  The ATmega328P has 1 USART, and we'll use
  // it as a normal asynchronous serial interfaces.
  //
  // USART0 -- command interface
  //
  // Set the USART baudrate registers. UBRR is a 12-bit register, so
  // it has a high and a low byte.
  //
  // Set baud rate
  // |--------+--------+----+--------+--------|
  // |   Baud | fosc   | 2x | UBRR0H | UBRR0L |
  // |--------+--------+----+--------+--------|
  // |   9600 | 16 MHz |  0 |      0 |    103 |
  // |  38400 | 16 MHz |  0 |      0 |     25 |
  // |  76800 | 16 MHz |  0 |      0 |     12 |
  // | 115200 | 16 MHz |  0 |      0 |      8 |
  // | 115200 | 16 MHz |  1 |      0 |     16 |
  // |--------+--------+----+--------+--------|

  //**************************** USART0 ****************************//

  // Set single speed
  UCSR0A &= ~(_BV(U2X0));

  // All our baud rates have a 0 UBRRnH
  UBRR0H = 0;

  // 38400 baud rate with 16 MHz crystal
  UBRR0L = 25;

  // Enable receiver and transmitter
  UCSR0B = _BV(RXEN0) | _BV(TXEN0);

  // Set asynchronous mode
  UCSR0C &= ~(_BV(UMSEL00)) & ~(_BV(UMSEL01));

  // Set no parity checking
  UCSR0C &= ~(_BV(UPM00)) & ~(_BV(UPM01));

  // Set 1 stop bit
  UCSR0C &= ~(_BV(USBS0));

  // Set 8 data bits
  UCSR0B &= ~(_BV(UCSZ02));
  UCSR0C |= _BV(UCSZ01) | _BV(UCSZ00);

  // Enable received character interrupts (command interface only)
  UCSR0B |= _BV(RXCIE0);

}
