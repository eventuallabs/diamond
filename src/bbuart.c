// Bit-banged uart module
//
// Taken from https://github.com/MarcelMG/AVR8_BitBang_UART_TX


//************************* Include files **************************//

#include <stdio.h>
#include <string.h>

// Convenience functions for busy-wait loops
#include <util/delay.h>

// Contains macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

// Library functions and macros for AVR interrupts
#include <avr/interrupt.h>

// Bit manipulation for AVR
#include "avr035.h"

// Provides PRINT_BUFFER_SIZE
#include "usart.h"

#include "bbuart.h"

// Dump data on this register to send it.
volatile uint16_t bbuart_data_reg = 0;

int8_t bbuart_init( void ) {
  // The bit-banged UART will use PB0 for Tx output.  Make this an
  // output, and make it idle high.
  SETBIT(BBUART_TX_DDR_REG, BBUART_TX_DDR_BIT);
  SETBIT(BBUART_TX_PORT_REG, BBUART_TX_PORT_BIT);

  // Set Timer0 to Clear Timer on Compare match (CTC) mode
  SETBIT(TCCR0A, WGM01);

  // Enable interrupts when timer reaches OCR0A
  SETBIT(TIMSK0, OCF0A);

  // 9600 baud is 104us per bit.  Our tick is 0.5us, so we need 208
  // ticks per bit.  Similar for other bauds.

  // |-------+-------+-------+----------|
  // |  Baud | Fosc  | OCR0A | Working? |
  // |-------+-------+-------+----------|
  // |  9600 | 16MHz |   208 | Yes      |
  // | 19200 | 16MHz |   104 | Yes      |
  // | 38400 | 16MHz |    52 | Yes      |
  // | 76800 | 16MHz |    28 | No       |
  // |-------+-------+-------+----------|
  OCR0A = 104;
  return 0;
}

int8_t bbuart_putc(char data) {
  uint16_t local_data_reg = bbuart_data_reg;

  // Tx is finished when bbuart_data_reg is 0.  If the transmitter
  // isn't finished, there's nothing we can do.
  while (bbuart_data_reg);

  // Set the (ninth) stop bit
  local_data_reg = (data << 1) | (1 << 9);

  // Dump the data on the uart data register
  bbuart_data_reg = local_data_reg;

  // The bbuart will use timer0 -- a 8-bit synchronous timer
  //
  // Configure the clock source for timer0 to be the system clock
  // scaled by 8.  This gives 0.5us per tick with a 16MHz clock.  This
  // configuration also starts the clock.
  SETBIT(TCCR0B, CS01);
  return 0;
}

int8_t bbuart_puts(char* data_ptr) {
  uint8_t i = 0;
  // Don't get stuck if a bad string shows up
  while( i < PRINT_BUFFER_SIZE) {
    if( data_ptr[i] == '\0' ) {
      // We've reached the string terminator, so we're done.  Stop and reset timer0.
      while (bbuart_data_reg);
      TCCR0B &= ~(7);
      TCNT0 = 0;
      // Make Tx idle high
      SETBIT(BBUART_TX_PORT_REG, BBUART_TX_PORT_BIT);
      break;
    }
    bbuart_putc( data_ptr[i++] );
  }
  return 0;
}

// Timer0 compare match interrupt
ISR(TIMER0_COMPA_vect) {
  uint16_t local_data_reg = bbuart_data_reg;

  // Output LSB of the Tx register at the Tx pin
  if (local_data_reg & 0x01) {
    // Bit is set
    SETBIT(BBUART_TX_PORT_REG, BBUART_TX_PORT_BIT);
  } else {
    CLEARBIT(BBUART_TX_PORT_REG, BBUART_TX_PORT_BIT);
  }
  // Get ready for the next interrupt
  local_data_reg >>= 1;
  bbuart_data_reg = local_data_reg;

  // If the stop bit has been sent, the shift register will be 0 and we're done.
  if (local_data_reg == 0) {
    // The stop bit has been sent, and we're done.  Stop and reset
    // Timer0
    TCCR0B = 0;
    TCNT0 = 0;
  }
}
