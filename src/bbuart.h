// Bit-banged UART -- Tx only

#ifndef BBUART_H
#define BBUART_H

// Define the pin used for Tx
#define BBUART_TX_PORT_REG PORTB
#define BBUART_TX_PORT_BIT PB0
#define BBUART_TX_DDR_REG DDRB
#define BBUART_TX_DDR_BIT DDB0

typedef struct bbuart_state_struct {

} bbuart_state_t;

// Initialize the module
int8_t bbuart_init(void);

// Send a character
int8_t bbuart_putc(char data);

// Send a string
int8_t bbuart_puts(char* data);

#endif // End the include guard
