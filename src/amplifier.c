#include <stdio.h>

// Provides macros for handling true and false arguments
#include <stdbool.h>

// Device-specific port definitions.  Also provides special
// bit-manipulations functions like bit_is_clear and
// loop_until_bit_is_set.
#include <avr/io.h>

// Provides macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

// Definitions of Two Wire Interface statuses
#include <util/twi.h>

// Convenience macros for setting and clearing bits
#include "avr035.h"

// Definitions common to i2c devices
#include "i2c.h"

// Provides logger_msg and logger_msg_p for log messages tagged with a
// system and severity.
#include "logger.h"

// Provides functions for setting and gettings system state variables
#include "system.h"

// Functions for maintaining a simple schedule
#include "OS.h"

#include "max9744.h"

#include "amplifier.h"

// MAX9744 I2C addresses
// |-------+-------+--------------|
// | ADDR2 | ADDR1 |      Address |
// |-------+-------+--------------|
// |     0 |     0 | I2C disabled |
// |     0 |     1 |         0x49 |
// |     1 |     0 |         0x4a |
// |     1 |     1 |         0x4b |
// |-------+-------+--------------|

int8_t amplifier_init(void) {
  int8_t retval = 0;
  logger_msg_p("amplifier", log_level_DEBUG, PSTR("Initializing amplifiers"));

  // Mute both amplifiers while we make sure their volumes are 0.
  // These mute functions will succeed even if the amplifiers are not present.
  amplifier_mute( AMPLIFIER_MODULE_WOOFER, true);
  amplifier_mute( AMPLIFIER_MODULE_TWEETER, true);

  
  // Test setting the volume to see if the amplifiers are present
  retval = amplifier_set_woofer_volume(0);
  if ( retval < 0 ) {
    logger_msg_p("amplifier", log_level_ERROR, PSTR("Woofer amplifier not found"));
    system_state_set_woofer_amplifier_present( false );
  } else {
    logger_msg_p("amplifier", log_level_DEBUG, PSTR("Woofer amplfier found"));
    system_state_set_woofer_amplifier_present( true );
  }

  retval = amplifier_set_tweeter_volume(0);
  if ( retval < 0 ) {
    logger_msg_p("amplifier", log_level_ERROR, PSTR("Tweeter amplifier not found"));
    system_state_set_tweeter_amplifier_present( false );
  } else {
    logger_msg_p("amplifier", log_level_DEBUG, PSTR("Tweeter amplfier found"));
    system_state_set_tweeter_amplifier_present( true );
  }

  // Unmute after setting 0 volume
  amplifier_mute( AMPLIFIER_MODULE_WOOFER, false);
  amplifier_mute( AMPLIFIER_MODULE_TWEETER, false);

  amplifier_shutdown( AMPLIFIER_MODULE_WOOFER, false );
  amplifier_shutdown( AMPLIFIER_MODULE_TWEETER, false );

  return 0;
}

int8_t amplifier_shutdown( amplifier_module_t amplifier, bool shutdown) {
  // The MAX9744 has an active-low shutdown pin.  Clearing the
  // configuration pin will shut down the amplifier.
  switch( amplifier ) {
  case AMPLIFIER_MODULE_WOOFER:

    if (shutdown) {
      // Shut down the woofer amplifier module
      CLEARBIT(PORTC, PORTC3);
    } else {
      // Bring the woofer module out of shutdown
      SETBIT(PORTC, PORTC3);
      logger_msg_p("amplifier", log_level_DEBUG, PSTR("Woofer brought out of shutdown"));
    }
    // Setting the data direction here saves us from having to get
    // the register right in the init function.
    SETBIT(DDRC, DDC3);
    break;

  case AMPLIFIER_MODULE_TWEETER:

    if (shutdown) {
      // Shut down the tweeter amplifier module
      CLEARBIT(PORTC, PORTC2);
    } else {
      // Bring the tweeter module out of shutdown
      SETBIT(PORTC, PORTC2);
    }
    // Setting the data direction here saves us from having to get
    // the register right in the init function.
    SETBIT(DDRC, DDC2);
    break;

  }
  return 0;
}

int8_t amplifier_mute( amplifier_module_t amplifier, bool mute) {
  // The MAX9744 has active-high mute pin.  Setting the configuration
  // bit will mute the amplifier.
  switch( amplifier ) {
  case AMPLIFIER_MODULE_WOOFER:

    if (mute) {
      // Mute the woofer amplifier module
      SETBIT(PORTB, PORTB2);
    } else {
      // Bring the woofer module out of mute
      CLEARBIT(PORTB, PORTB2);
    }
    // Setting the data direction here saves us from having to get
    // the register right in the init function.
    SETBIT(DDRB, DDB2);
    break;

  case AMPLIFIER_MODULE_TWEETER:

    if (mute) {
      // Shut down the tweeter amplifier module
      SETBIT(PORTB, PORTB1);
    } else {
      // Bring the tweeter module out of mute
      CLEARBIT(PORTB, PORTB1);
    }
    // Setting the data direction here saves us from having to get
    // the register right in the init function.
    SETBIT(DDRB, DDB1);
    break;

  }
  return 0;
}

int8_t amplifier_set_tweeter_volume( uint8_t level ) {
  logger_msg_p("amplifier", log_level_DEBUG, PSTR("Setting tweeter amplifier volume to %u"), level);
  int8_t retval = 0;

  // The MAX9744 amplifier accepts volume levels 0-63
  if ( (level < 0) || (level > 63) ) {
    logger_msg_p("amplifier", log_level_ERROR, PSTR("Volume setting of %u out of range for tweeter"),
		 level);
    return -1;
  }

  // Set tweeter amplifier volume
  retval = max9744_write(AMPLIFIER_TWEETER_I2C_ADDRESS, level);
  return retval;
}

int8_t amplifier_set_woofer_volume( uint8_t level ) {
  logger_msg_p("amplifier", log_level_DEBUG, PSTR("Setting woofer amplifier volume to %u"), level);
  int8_t retval = 0;

  // The MAX9744 amplifier accepts volume levels 0-63
  if ( (level < 0) || (level > 63) ) {
    logger_msg_p("amplifier", log_level_ERROR, PSTR("Volume setting of %u out of range for woofer"),
		 level);
    return -1;
  }

  // Set woofer amplifier volume
  retval = max9744_write(AMPLIFIER_WOOFER_I2C_ADDRESS, level);
  return retval;
}
