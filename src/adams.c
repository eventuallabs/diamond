#include <stdio.h>

// Device-specific port definitions.  Also provides special
// bit-manipulations functions like bit_is_clear and
// loop_until_bit_is_set.
#include <avr/io.h>

// Provides macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

// Convenience functions for busy-wait loops
#include <util/delay.h>

// Functions for working with UART interfaces.
// Definition of LINE_TERMINATION_CHARACTERS
#include "usart.h"

// Provides logger_msg and logger_msg_p for log messages tagged with a
// system and severity.
#include "logger.h"

// Provides setter and getter functions for the system state structure
#include "system.h"

// Provides spi_init()
#include "spi.h"

#include "adams.h"

adams_state_t adams_state;

int8_t adams_init(void) {
  int8_t retval = 0;

  // Adams uses the SPI module
  spi_init();

  // Make PC1 an output initialized high for (not) output enable
  PORTC |= _BV(PORTC1);
  DDRC |= _BV(DDC1);

  // Make PD6 an output initialized high for (not) chip select
  PORTD |= _BV(PORTD6);
  DDRD |= _BV(DDD6);

  // Make PD3 and output initialized high for front panel PWM
  PORTD |= _BV(PORTD3);
  DDRD |= _BV(DDD3);

  // Set initial value for OCR2B.  Higher values will increase the
  // duty cycle, making the output high for more of the period and
  // making the front panel brighter.
  adams_set_brightness( 100 );

  // Clear OC2B on compare match, set OC2B when counter overflows
  TCCR2A |= _BV(COM2B1);

  // Fast PWM with the counter TOP = 0xff
  TCCR2A |= ( _BV(WGM21) | _BV(WGM20) );

  // Clock Timer 2 with a prescaler of 256 to get a 244 Hz PWM
  // frequency.  This also starts Timer 2.
  TCCR2B |= ( _BV(CS22) | _BV(CS21) );

  // Reset compare match flag by writing a 1 to it
  TIFR2 = _BV(OCF2B);

  // Initialize the board
  adams_write(0UL);

  // Bring output enable low
  adams_set_noe(0);

  // Breathing with start with increasing brightness
  adams_state.getting_brighter = true;

  return retval;
}

int8_t adams_set_cs( uint8_t data ) {
  int8_t retval = 0;
  switch( data ) {
  case 1:
    PORTD |= _BV(PORTD6);
    break;
  case 0:
    PORTD &= ~(_BV(PORTD6));
    break;
  default:
    break;
  }
  return retval;
}

int8_t adams_set_noe( uint8_t data ) {
  int8_t retval = 0;
  switch( data ) {
  case 1:
    PORTC |= _BV(PORTC1);
    break;
  case 0:
    PORTC &= ~(_BV(PORTC1));
    break;
  default:
    break;
  }
  return retval;
}

int8_t adams_set_brightness( uint8_t brightness ) {
  int8_t retval = 0;
  if ( brightness == 0 ) {
    // Simply turn the LEDs off
    adams_write( (uint64_t) 0 );
  }
  if ( OCR2B == 0 ) {
    // We have blanked the LEDs, so restore them first
    adams_write( adams_state.fp_leds_value );
  }
  OCR2B = brightness;
  return retval;
}

uint8_t adams_get_brightness( void ) {
  return OCR2B;
}

void adams_increment_breathe( void ) {
  uint8_t current_brightness = adams_get_brightness();
  if ( current_brightness >= KNOB_BREATHE_MAX ) {
    adams_set_brightness( KNOB_BREATHE_MAX );
    current_brightness = KNOB_BREATHE_MAX;
    adams_state.getting_brighter = false;
  }
  if ( current_brightness == KNOB_BREATHE_MIN ) {
    adams_state.getting_brighter = true;
  }
  if (adams_state.getting_brighter) {
    adams_set_brightness( current_brightness + 1 );
  } else {
    adams_set_brightness( current_brightness - 1 );
  }
  return;
}

int8_t adams_set_leds( uint64_t data ) {
  int8_t retval = 0;

  // Copy the value to the status structure
  adams_state.fp_leds_value = data;

  // Write the value to the hardware
  retval = adams_write( data );

  return retval;
}

int8_t adams_track_volume( uint16_t volume_counts ) {
  int8_t retval = 0;
  if ( volume_counts < KNOB_LED_0_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_1_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_2_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_3_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_4_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_5_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 |
		    KNOB_LED_5 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_6_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 |
		    KNOB_LED_5 | KNOB_LED_6 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_7_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 |
		    KNOB_LED_5 | KNOB_LED_6 | KNOB_LED_7 );
    return retval;
  }
  if ( volume_counts < KNOB_LED_8_THRESHOLD ) {
    adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 |
		    KNOB_LED_5 | KNOB_LED_6 | KNOB_LED_7 | KNOB_LED_8 );
    return retval;
  }
  adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 |
		  KNOB_LED_5 | KNOB_LED_6 | KNOB_LED_7 | KNOB_LED_8 | KNOB_LED_9 );
  
  return retval;
}

int8_t adams_write( uint64_t data ) {
  int8_t retval = 0;

  union {
    uint8_t b[8];
    uint64_t w;
  } data_union;

  data_union.w = data;

  // Pull chip-select low
  adams_set_cs(0);

  for (int8_t bytenum = 7; bytenum >= 0; bytenum--) {
    spi_write( data_union.b[bytenum] );
    // logger_msg_p("adams", log_level_DEBUG, PSTR("Wrote 0x%x"),data_union.b[bytenum]);
  }
  // This delay has to stay to prevent cs from coming up early
  _delay_ms(1);
  // Return chip-select high
  adams_set_cs(1);
  return retval;
}

int8_t adams_set_all_knob_leds( void ) {
  int8_t retval = 0;
  retval = adams_set_leds( KNOB_LED_0 | KNOB_LED_1 | KNOB_LED_2 | KNOB_LED_3 | KNOB_LED_4 |
			   KNOB_LED_5 | KNOB_LED_6 | KNOB_LED_7 | KNOB_LED_8 | KNOB_LED_9 );
  return retval;
}
