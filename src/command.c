/* bx_command.c */

// ----------------------- Include files ------------------------------
#include <stdio.h>
#include <string.h>

/* pgmspace.h

   Provides macros and functions for saving and reading data out of
   flash. */
#include <avr/pgmspace.h>

/* command.h

   Provides the extern declaration of command_array -- an array
   containing all the commands understood by the system. */
#include "command.h"

// Provides some sample functions for the command handler to call.
#include "system.h"

/* usart.h

   Provides functions for transmitting characters over the usart. */
#include "usart.h"

/* ascii.h

   Provides lowstring() for converting strings to lower case. */
#include "ascii.h"

/* logger.h

   Provides logger_msg and logger_msg_p for log messages tagged with a
   system and severity. */
#include "logger.h"

// Provides ascii to number conversion
#include "numbers.h"

// Provides commands to work with the eeprom
#include "eeprom.h"

/* Define the remote commands recognized by the system.
 */
command_t command_array[] = {
  // *IDN? -- Print the instrument identification string.
  {"*idn?",           // Name of the command
   "none",            // Argument type (none, hex16, uint16, sint16)
   0,                 // Maximum number of characters in argument
   &cmd_idn_q},       // Address of function to execute
  // *RST -- System reset
  {"*rst",
   "none",
   0,
   &cmd_rst},
  //loglev -- Set the logger severity level.
  {"loglev",
   "uint16",
   1,
   &cmd_loglevel},
  // logreg -- Set the logger enable register.
  {"logreg",
   "uint16",
   4,
   &cmd_logreg},
  // logreg? -- Query the logger enable register.
  {"logreg?",
   "none",
   0,
   &cmd_logreg_q},
  // sernum -- Set the system's serial number
  {"sernum",
   "uint16",
   5,
   &cmd_write_sernum},
  // tmute -- Mute or unmute the tweeters
  {"tmute",
   "uint16",
   1,
   &cmd_tmute},
  // wmute -- Mute or unmute the woofer
  {"wmute",
   "uint16",
   1,
   &cmd_wmute},
    // wmute? -- Query the woofer's muted state
  {"wmute?",
   "none",
   0,
   &cmd_wmute_q},
  // tmute? -- Query the tweeters muted state
  {"tmute?",
   "none",
   0,
   &cmd_tmute_q},
  // woff -- Set the woofer volume offset
  {"woff",
   "uint16",
   2,
   &cmd_woff},
  // woff? -- Query the woofer volume offset
  {"woff?",
   "none",
   0,
   &cmd_woff_q},
  // toff -- Set the tweeter volume offset
  {"toff",
   "uint16",
   2,
   &cmd_toff},
  // toff? -- Query the tweeter volume offset
  {"toff?",
   "none",
   0,
   &cmd_toff_q},
  // End of table indicator.  Must be last.
  {"","",0,0}
};

/* Declare a structure to hold function arguments
 */
command_arg_t command_arg;
command_arg_t *command_arg_ptr = &command_arg;

// ----------------------- Functions ----------------------------------

/* command_init( received command state pointer )

   Making this function explicitly take a pointer to the received
   command state structure makes it clear that it modifies this
   structure.

*/
void command_init( recv_cmd_state_t *recv_cmd_state_ptr ) {
    memset((recv_cmd_state_ptr -> rbuffer),0,RECEIVE_BUFFER_SIZE);
    recv_cmd_state_ptr -> rbuffer_write_ptr =
        recv_cmd_state_ptr -> rbuffer; // Initialize write pointer
    memset((recv_cmd_state_ptr -> pbuffer),0,RECEIVE_BUFFER_SIZE);
    recv_cmd_state_ptr -> pbuffer_arg_ptr =
        recv_cmd_state_ptr -> pbuffer; // Initialize argument pointer
    recv_cmd_state_ptr -> rbuffer_count = 0;
    recv_cmd_state_ptr -> pbuffer_locked = false; // Parse buffer unlocked
    return;
}

// Returns -1 when argument sizes are too big or when an argument was
// expected and not recevied.  Returns 0 otherwise.
uint8_t check_argsize(recv_cmd_state_t *recv_cmd_state_ptr ,
                      struct command_struct *command_array) {
  uint8_t argsize = strlen(recv_cmd_state_ptr -> pbuffer_arg_ptr);
  if ( command_array -> arg_max_chars > 0 ) {
    // An argument is expected
    logger_msg_p("command",log_level_INFO,
		 PSTR("Argument size is %d, 0 < expected < %d"), argsize, command_array -> arg_max_chars);
    if ( argsize > 0 && argsize <= command_array -> arg_max_chars ) {
      return 0;
    } else {
      return -1;
    }
  } else {
    logger_msg_p("command",log_level_INFO, PSTR("Argument size is %d, expected 0"));
    if ( argsize == 0 ) {
      return 0;
    } else {
      return -1;
    }
  }
}

void rbuffer_erase( recv_cmd_state_t *recv_cmd_state_ptr ) {
    memset((recv_cmd_state_ptr -> rbuffer),0,RECEIVE_BUFFER_SIZE);
    // Initialize write pointer
    recv_cmd_state_ptr -> rbuffer_write_ptr = recv_cmd_state_ptr -> rbuffer;
    recv_cmd_state_ptr -> rbuffer_count = 0;
    return;
}

void command_process_pbuffer( recv_cmd_state_t *recv_cmd_state_ptr ,
			      struct command_struct *command_array ) {
  char *command_space_ptr;
  // Process the commmand (if there is one) in the parse buffer.
  if ((recv_cmd_state_ptr -> pbuffer_locked) == true) {
    // Parse buffer is locked -- there's a command to process
    logger_msg_p("command",log_level_INFO, PSTR("The parse buffer is locked."));
    command_space_ptr = strchr(recv_cmd_state_ptr -> pbuffer,' ');
    if ( command_space_ptr == NULL ) {
      // There was no incoming argument
      logger_msg_p("command",log_level_INFO, PSTR("No incoming argument."));

      // Move the parse buffer pointer to the end of the command
      recv_cmd_state_ptr -> pbuffer_arg_ptr = strchr(recv_cmd_state_ptr -> pbuffer,'\0');
    } else {
      // Parse buffer contains a space -- there's an argument.  Move
      // the parse buffer pointer to the beginning of the argument.
      recv_cmd_state_ptr -> pbuffer_arg_ptr = command_space_ptr;
      logger_msg_p("command",log_level_INFO,
		   PSTR("The command contains a space."));
      // Terminate the command string
      *(recv_cmd_state_ptr -> pbuffer_arg_ptr) = '\0';
      (recv_cmd_state_ptr -> pbuffer_arg_ptr)++;
      while (*(recv_cmd_state_ptr -> pbuffer_arg_ptr) == ' ') {
	// Move to first non-space character
	(recv_cmd_state_ptr -> pbuffer_arg_ptr)++;
      }
      // pbuffer_arg_ptr now points to the beginning of the argument
      logger_msg_p("command",log_level_INFO,
		   PSTR("The incoming argument is '%s'."),
		   (recv_cmd_state_ptr -> pbuffer_arg_ptr));
    }
    // Convert command to lower case
    lowstring(recv_cmd_state_ptr -> pbuffer);
    // Look through the command list for a match
    uint8_t pbuffer_match = 0;
    while ((command_array -> execute) != 0) {
      if (strcmp( recv_cmd_state_ptr -> pbuffer, command_array -> name ) == 0) {
	// We've found a matching command
	logger_msg_p("command",log_level_INFO,
		     PSTR("Command '%s' recognized."),command_array -> name);
	pbuffer_match = 1;
	if (strcmp( command_array -> arg_type, "none") != 0) {
	  // The command is specified to have an argument
	  uint8_t arg_ok = check_argsize(recv_cmd_state_ptr,command_array);
	  if (arg_ok != 0) {
	    // The argument is too large
	    logger_msg_p("command",log_level_ERROR,
			 PSTR("Argument to '%s' is out of range."),
			 command_array -> name);
	    command_nack(NACK_ARGUMENT_OUT_OF_RANGE);
	    recv_cmd_state_ptr -> pbuffer_locked = false;
	    return;
	  }
	  else {
	    // The argument is the right size
	    logger_msg_p("command",log_level_INFO,
			 PSTR("Argument to '%s' is within limits."),
			 command_array -> name);
	    command_exec(command_array,recv_cmd_state_ptr -> pbuffer_arg_ptr,
			 command_arg_ptr);
	  }
	}
	else  {
	  // There's no argument specified
	  if (command_space_ptr != NULL) {
	    // There's an argument, but we didn't expect one
	    logger_msg_p("command",log_level_WARNING,
			 PSTR("Ignoring argument for command '%s'."),
			 command_array -> name);
	  }
	  command_exec(command_array, NULL, command_arg_ptr);
	}
	recv_cmd_state_ptr -> pbuffer_locked = false;
	break;
      }
      command_array++;
    }
    // If we didn't find a match, send an error message to the logger
    // and a NACK to the command interface.
    if (pbuffer_match == 0) {
      logger_msg_p("command",log_level_ERROR,
		   PSTR("Unrecognized command: '%s'."),
		   recv_cmd_state_ptr -> pbuffer);
      command_nack(NACK_COMMAND_NOT_RECOGNIZED);
      recv_cmd_state_ptr -> pbuffer_locked = false;
    }
  }
  return;
}

/* command_exec( remote command string, argument string,
                 command argument structure pointer )

   Execute a valid command received over the remote interface.  The
   command's arguments simply come in as strings.  The command's
   definition sets the argument type.  This function matches a string
   to that argument type string to figure out how to convert the
   argument string to a number.
*/
void command_exec( command_t *command, char *argument,
		   command_arg_t *command_arg_ptr ) {
  if (strcmp( command -> arg_type,"none" ) == 0) {
    // There's no argument
    logger_msg_p("command",log_level_INFO,
		 PSTR("Executing command with no argument."));
    command -> execute(command_arg_ptr);
  }
  else if (strcmp( command -> arg_type,"hex16" ) == 0) {
    // There's a hex argument
    logger_msg_p("command",log_level_INFO,
		 PSTR("Executing command with hex argument."));
    command_arg_ptr -> uint16_arg = hex2num(argument);

    logger_msg_p("command",log_level_INFO,
		 PSTR("The argument value is %u."),
		 command_arg_ptr -> uint16_arg);

    command -> execute(command_arg_ptr);
  }
  else if (strcmp( command -> arg_type,"uint16" ) == 0) {
    // There's a unsigned 16-bit integer argument
    logger_msg_p("command",log_level_INFO,
		 PSTR("Executing command with unsigned int argument."));
    command_arg_ptr -> uint16_arg = uint2num(argument);
    command -> execute(command_arg_ptr);
  }
  else if (strcmp( command -> arg_type,"sint16" ) == 0) {
    // There's a signed 16-bit integer argument
    logger_msg_p("command",log_level_INFO,
		 PSTR("Executing command with signed int argument."));
    command_arg_ptr -> sint16_arg = sint2num(argument);
    command -> execute(command_arg_ptr);
  }
  /* If we've reached the end, we haven't found a match for the
     command's argument type.  That's an error.
   */
  else {
    logger_msg_p("command",log_level_ERROR,
		 PSTR("No handler specified for %s argument."),
		 command -> arg_type);
  }
}

void command_ack() {
  usart_printf( "0%s", LINE_TERMINATION_CHARACTERS );
}

void command_nack( int8_t nack_code ) {
  usart_printf( "%i%s", nack_code, LINE_TERMINATION_CHARACTERS );
}
