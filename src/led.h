#ifndef LED_H
#define LED_H

// Provides boolean datatypes
#include <stdbool.h>

// All OS timeouts are 32-bit milliseconds
#define LED_DEBUG_PULSE_MS UINT32_C(200)

// The debug LED is not the same as the Arduino LED, since the Arduino
// LED is shared with the SPI bus.
#define LED_DEBUG_PORT_REG PORTD
#define LED_DEBUG_PORT_BIT PD5
#define LED_DEBUG_DDR_REG DDRD
#define LED_DEBUG_DDR_BIT DDD5

typedef struct led_state_struct {

  // Debug LED off task
  uint8_t debug_led_off_task_number;

  // Debug LED on
  bool debug_led_on;

  // Debug LED sequence position
  uint8_t debug_led_array_index;

} led_state_t;

// Sets the port pin directions for LEDs to be outputs.
void led_init(void);

// Set the debug LED
void led_set_debug_led(bool setting);

// Turn the debug LED off
void led_debug_led_off_task(void);

// Pulse the debug LED
void led_pulse_debug_led(void);



#endif // End the include guard
