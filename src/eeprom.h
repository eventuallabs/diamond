
#ifndef EEPROM_H
#define EEPROM_H

// Provides definition of system_state_t
#include "system.h"

// Define the offset for general system variables. The eeprom has 1000
// total locations.  I'll allocate addresses 0-99 for general system
// storage.

// The serial number is two bytes wide -- occupying addresses 0 and 1.
// The next open address is 2.
#define SERNUM_ADDR 0

// Woofer volume offset.  This will be one byte.
#define WOOFER_OFFSET_ADDR 2

// Tweeter volume offset.  This will be one byte
#define TWEETER_OFFSET_ADDR 3

// Write an 8-bit value to an eeprom address
void eeprom_write_char( uint16_t address, uint8_t data );

// Return an 8-bit unsigned integer from an eeprom address
uint8_t eeprom_read_char( uint16_t address );

// Write the serial number
void eeprom_save_sernum( uint16_t );

// Load the serial number into the system state structure
void eeprom_load_sernum( system_state_t *system_state_ptr );

// Write the woofer volume offset
//
// Arguments:
//   offset -- Integer added to the volume knob for the woofer
int8_t eeprom_write_woofer_offset( uint8_t offset);

// Return the woofer volume offset
uint8_t eeprom_read_woofer_offset( void );

// Write the tweeter volume offset
//
// Arguments:
//   offset -- Integer added to the volume knob for the tweeter
int8_t eeprom_write_tweeter_offset( uint8_t offset);

// Return the tweeter volume offset
uint8_t eeprom_read_tweeter_offset( void );

//************************ Remote commands *************************//

// Remote command to set the instrument's serial number
void cmd_write_sernum( command_arg_t *command_arg_ptr );

#endif // End the include guard
