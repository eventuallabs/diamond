// The MAX9744 20W audio amplifier from Maxim

#ifndef MAX9744_H
#define MAX9744_H

int8_t max9744_init( uint8_t slave_address );

// The max7944 is a write-only device
int8_t max9744_write( uint8_t slave_address, uint8_t data );



#endif
