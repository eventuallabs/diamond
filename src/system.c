// Miscellaneous system functions

// ----------------------- Include files ------------------------------
#include <stdio.h>
#include <string.h>

// Contains macros and functions for saving and reading data out of flash.
#include <avr/pgmspace.h>

// Watchdog timer
#include <avr/wdt.h>

#include "usart.h"

// Functions for simple scheduling
#include "OS.h"

// Provides logging functions
#include "logger.h"

// Provides commands for working with the eeprom
#include "eeprom.h"

// Functions for maintaining a simple schedule
#include "OS.h"

// Provides StackCount for reporting unused RAM bytes
#include "stackmon.h"

// Provides command_ack and command_nack for remote commands
#include "command.h"

// Provides led-pulsing functions
#include "led.h"

// Provides commands for writing to SPI
#include "spi.h"

// Provides volume_read() for reading the volume knob
#include "volume.h"

// Provides amplifier_set_woofer_volume() and amplifier_set_tweeter_volume()
#include "amplifier.h"

// Provides functions for working with the front panel
#include "adams.h"

#include "system.h"

// ----------------------- Globals ------------------------------------

// idnstr format: Organization, model, serial, version
const char idnstr[] PROGMEM = "Eventual Labs,Diamond,SN%u,%s";

/* System status structure

   Keeps track of miscellaneous system status:
   -- Serial number
*/
system_state_t system_state;
system_state_t *system_state_ptr = &system_state;

// ----------------------- Functions ----------------------------------

void system_init( void ) {
  eeprom_load_sernum(system_state_ptr);

  system_state.woofer_volume_offset = eeprom_read_woofer_offset();
  logger_msg_p("system", log_level_DEBUG, PSTR("Loaded woofer offset of %u"),
		 system_state.woofer_volume_offset);

  system_state.tweeter_volume_offset = eeprom_read_tweeter_offset();
  logger_msg_p("system", log_level_DEBUG, PSTR("Loaded tweeter offset of %u"),
		 system_state.tweeter_volume_offset);

  // Enable all speakers
  system_set_woofer_muted( false );
  system_set_tweeters_muted( false );

  // Set the initial state
  system_state.state_enum = system_state_INIT;

  // Set the initial volume knob position at 0 so that the volume task
  // will detect a change.
  system_state.volume_knob_position = 0;

  // Create and schedule a task for breathing the front panel LEDs
  //
  // OS_TaskCreate(function pointer, interval (ms), READY, BLOCKED or SUSPENDED)
  //
  // READY tasks will execute ASAP and then switch to BLOCKED
  // BLOCKED tasks will wait for their interval to expire and then become READY
  // SUSPENDED tasks will never execute
  OS_TaskCreate( &system_breathe_task, 10, BLOCKED );
  system_state.breathe_leds_task_number = OS_get_task_number( &system_breathe_task );

  OS_TaskCreate(&system_exit_turning_task, 1000, SUSPENDED);
  system_state.exit_turning_task_number = OS_get_task_number( &system_exit_turning_task );

  // Start the system heartbeat LED
  OS_TaskCreate( &system_heartbeat_task, 1000, BLOCKED );

  // Start the volume check task
  OS_TaskCreate( &system_volume_task, VOLUME_KNOB_CHECK_INTERVAL_MS, BLOCKED );

  if (strcmp( LOG_LEVEL, "debug" ) == 0) {
    // Start the task to monitor free memory    
    OS_TaskCreate(&system_report_stack_free, 5000, BLOCKED);
  }

  // Jump into the turning state to light all LEDs up to the volume
  // level.  This has to happen after the tasks have been configured.
  system_enter_turning();
}

void system_print_identity( void ) {
  usart_printf_p( idnstr, system_state.sernum, REVCODE );
  usart_printf( LINE_TERMINATION_CHARACTERS );
}

void system_heartbeat_task( void ) {
  led_pulse_debug_led();
}

void system_volume_task( void ) {
  uint16_t volume_knob_position = volume_read();
  uint8_t woofer_volume_setting = 0;
  uint8_t tweeter_volume_setting = 0;

  if ( volume_knob_position <= VOLUME_NOISE_COUNTS ) {
    // We're within the noise of zero.
    volume_knob_position = 0;
    system_enter_ccw();
  }

  if ( (int16_t) volume_knob_position < (int16_t) (system_state.volume_knob_position - VOLUME_NOISE_COUNTS ) ||
       (int16_t) volume_knob_position > (int16_t) (system_state.volume_knob_position + VOLUME_NOISE_COUNTS )) {
    // The volume knob has moved
    logger_msg_p("system", log_level_INFO, PSTR("Changing volume knob from %i to %i"),
		 system_state.volume_knob_position, volume_knob_position);
    system_state.volume_knob_position = volume_knob_position;
    system_enter_turning();
  } else {
    // The volume knob has not moved.  Do not reset the exit_turing task so it can execute.
    return;
  }
  // The MAX9744 amplifiers have 64 (2**6) settings.  The max volume
  // knob is 2**10.  Simply bitshift by 4 to convert knob position to
  // volume setting.
  woofer_volume_setting = system_state.woofer_volume_offset + (volume_knob_position >> 4);
  if ( woofer_volume_setting > AMPLIFIER_MAX_VOLUME ) {
    woofer_volume_setting = AMPLIFIER_MAX_VOLUME;
  }

  tweeter_volume_setting = system_state.tweeter_volume_offset + (volume_knob_position >> 4);
  if ( tweeter_volume_setting > AMPLIFIER_MAX_VOLUME ) {
    tweeter_volume_setting = AMPLIFIER_MAX_VOLUME;
  }

  if ( system_state.woofer_amplifier_present && !(system_state.woofer_muted) ) {
    amplifier_set_woofer_volume( woofer_volume_setting );
  }
  if ( system_state.tweeter_amplifier_present && !(system_state.tweeters_muted) ) {
    amplifier_set_tweeter_volume( tweeter_volume_setting );
  }
  return;
}

void system_enter_ccw( void ) {
  switch( system_state.state_enum ) {

  case system_state_INIT:
    // We got here from powerup or reset.  Set the lowest LED
    adams_set_leds( KNOB_LED_0 );
    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter ccw from  state %d"),
		 system_state.state_enum);
    set_system_state( system_state_CCW );
    break;
  case system_state_CCW:
    // We're already in this state
    break;
  case system_state_TURNING:
    adams_set_leds( KNOB_LED_0 );

    // Start breathing the LEDs
    system_start_breathing();

    logger_msg_p("system", log_level_DEBUG, PSTR("Enter ccw from state %d"), system_state.state_enum);
    set_system_state( system_state_CCW );
    break;
  case system_state_UNMUTED:
    adams_set_leds( KNOB_LED_0 );
    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter ccw from state %d"),
		 system_state.state_enum);
    set_system_state( system_state_CCW );
    break;
  default:
    logger_msg_p("system", log_level_ERROR,
		 PSTR("Enter ccw from bad system state %d"),
		 system_state.state_enum);
    break;
  }
  return;
}

void system_enter_turning( void ) {
  switch( system_state.state_enum ) {

  case system_state_INIT:
    // Stop breathing the LEDs
    system_stop_breathing();

    // Schedule the exit task
    OS_SetTaskState( system_state.exit_turning_task_number, BLOCKED );

    adams_track_volume( system_state.volume_knob_position );
    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter TURNING from state INIT"),
		 system_state.state_enum);
    set_system_state( system_state_TURNING );
    break;
  case system_state_CCW:
    // Stop breathing the LEDs
    system_stop_breathing();

    // Schedule the exit task
    OS_SetTaskState( system_state.exit_turning_task_number, BLOCKED );
    adams_track_volume( system_state.volume_knob_position );
    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter turning from state %d"),
		 system_state.state_enum);
    set_system_state( system_state_TURNING );
    break;
  case system_state_TURNING:
    adams_track_volume( system_state.volume_knob_position );
    // Reset the exit task timer
    OS_SetTaskCntTime( system_state.exit_turning_task_number, 0 );
    break;
  case system_state_UNMUTED:
    // Stop breathing the LEDs
    system_stop_breathing();

    // Schedule the exit task
    OS_SetTaskState( system_state.exit_turning_task_number, BLOCKED );

    // Light LEDs up to the volume level
    adams_track_volume( system_state.volume_knob_position );

    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter TURNING from state UNMUTED"),
		 system_state.state_enum);
    set_system_state( system_state_TURNING );
    break;
  default:
    logger_msg_p("system", log_level_ERROR,
		 PSTR("Enter ccw from bad system state %d"),
		 system_state.state_enum);
    break;
  }
  return;
}

void system_enter_unmuted( void ) {
  switch( system_state.state_enum ) {

  case system_state_INIT:
    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter UNMUTED from state INIT"),
		 system_state.state_enum);
    set_system_state( system_state_UNMUTED );

    // We want to light LEDs up to the volume level at startup, and
    // this is what happens in the TURNING state.
    system_enter_turning();
    break;
  case system_state_CCW:
    logger_msg_p("system", log_level_DEBUG,
		 PSTR("Enter unmuted from state %d"),
		 system_state.state_enum);
    set_system_state( system_state_UNMUTED );
    break;
  case system_state_TURNING:
    // Turn all LEDs on
    adams_set_all_knob_leds();

    // Start breathing the LEDs
    system_start_breathing();

    logger_msg_p("system", log_level_DEBUG, PSTR("Enter UNMUTED from state TURNING"));
    set_system_state( system_state_UNMUTED );
    break;
  case system_state_UNMUTED:
    break;
  default:
    logger_msg_p("system", log_level_ERROR,
		 PSTR("Enter unmuted from bad system state %d"),
		 system_state.state_enum);
    break;
  }
  return;
}

void system_exit_turning_task( void ) {
  // The volume knob is stable
  if ( system_state.volume_knob_position == 0 ) {
    system_enter_ccw();
  } else {
    system_enter_unmuted();
  }
}

int8_t system_start_breathing( void ) {
  int8_t retval = 0;

  // Un-suspend the breathe task
  OS_SetTaskState(system_state.breathe_leds_task_number, BLOCKED);
  return retval;
}

int8_t system_stop_breathing( void ) {
  int8_t retval = 0;

  // Cancel the breathing task
  OS_SetTaskState(system_state.breathe_leds_task_number, SUSPENDED);

  // Reset the LED brightness
  retval += adams_set_brightness( KNOB_TURNING_BRIGHTNESS );

  return retval;
}

void system_breathe_task( void ) {
  adams_increment_breathe();
}

void cmd_idn_q( command_arg_t *command_arg_ptr ) {
  usart_printf_p( idnstr, system_state_ptr -> sernum,REVCODE );
  usart_printf( LINE_TERMINATION_CHARACTERS );
}

system_state_value_t set_system_state(system_state_value_t requested_state) {
  // For now, just set the state to the requested value
  system_state.state_enum = requested_state;
  return(system_state.state_enum);
}

system_state_value_t get_system_state( void ) {
  // Return the current system state
  return(system_state_ptr -> state_enum);
}

void cmd_rst( command_arg_t *command_arg_ptr ) {
  // Set watchdog to lowest interval and delay longer than that
  wdt_enable(WDTO_15MS);
  while(1);
}

system_report_stack_free( void ) {
  uint16_t free_bytes = StackCount();
  logger_msg_p("system", log_level_DEBUG,
		 PSTR("%u bytes of RAM unused"),
		 free_bytes);
}

void cmd_opstate_q( command_arg_t *command_arg_ptr ) {
  switch( system_state_ptr -> state_enum ) {
  case system_state_INIT:
    usart_printf( "%s%s", "init", LINE_TERMINATION_CHARACTERS );
    break;
  case system_state_CCW:
    usart_printf( "%s%s", "ccw", LINE_TERMINATION_CHARACTERS );
    break;
  default:
    usart_printf( "%s%s", "none", LINE_TERMINATION_CHARACTERS );
  }
}

int8_t system_state_set_woofer_amplifier_present( bool present ) {
  if ( present ) {
    system_state.woofer_amplifier_present = true;
  } else {
    system_state.woofer_amplifier_present = false;
  }
  return 0;
}

bool system_state_get_woofer_amplifier_present(void) {
  return system_state.woofer_amplifier_present;
}

bool system_state_get_tweeter_amplifier_present(void) {
  return system_state.tweeter_amplifier_present;
}

int8_t system_state_set_tweeter_amplifier_present( bool present ) {
  if ( present ) {
    system_state.tweeter_amplifier_present = true;
  } else {
    system_state.tweeter_amplifier_present = false;
  }
  return 0;
}

int8_t system_set_tweeters_muted( bool mute ) {
  if ( mute ) {
    system_state.tweeters_muted = true;
    amplifier_mute( AMPLIFIER_MODULE_TWEETER, true );
  } else {
    system_state.tweeters_muted = false;
    amplifier_mute( AMPLIFIER_MODULE_TWEETER, false );
  }
  return 0;
}

bool system_get_tweeters_muted( void ) {
  if ( system_state.tweeters_muted ) {
    return true;
  } else {
    return false;
  }
}

int8_t system_set_woofer_muted( bool mute ) {
  if ( mute ) {
    system_state.woofer_muted = true;
    amplifier_mute( AMPLIFIER_MODULE_WOOFER, true );
  } else {
    system_state.woofer_muted = false;
    amplifier_mute( AMPLIFIER_MODULE_WOOFER, false );
  }
  return 0;
}

bool system_get_woofer_muted( void ) {
  if ( system_state.woofer_muted ) {
    return true;
  } else {
    return false;
  }
}

void cmd_tmute( command_arg_t *command_arg_ptr ) {
  int8_t retval = 0;
  uint16_t mute = ( command_arg_ptr -> uint16_arg );
  if ( mute ) {
    retval += system_set_tweeters_muted( true );
  } else {
    retval += system_set_tweeters_muted( false );
  }
  if (retval == 0) {
    command_ack();
    return;
  } else {
    command_nack(NACK_COMMAND_FAILED);
    return;
  }
}

void cmd_tmute_q( command_arg_t *command_arg_ptr ) {
    if ( system_state.tweeters_muted ) {
      usart_printf( "muted%s", LINE_TERMINATION_CHARACTERS );
  } else {
      usart_printf( "unmuted%s", LINE_TERMINATION_CHARACTERS );
  }
}

void cmd_wmute( command_arg_t *command_arg_ptr ) {
  int8_t retval = 0;
  uint16_t mute = ( command_arg_ptr -> uint16_arg );
  if ( mute ) {
    retval += system_set_woofer_muted( true );
  } else {
    retval += system_set_woofer_muted( false );
  }
  if (retval == 0) {
    command_ack();
    return;
  } else {
    command_nack(NACK_COMMAND_FAILED);
    return;
  }
}

void cmd_wmute_q( command_arg_t *command_arg_ptr ) {
    if ( system_state.woofer_muted ) {
      usart_printf( "muted%s", LINE_TERMINATION_CHARACTERS );
  } else {
      usart_printf( "unmuted%s", LINE_TERMINATION_CHARACTERS );
  }
}

void cmd_woff( command_arg_t *command_arg_ptr ) {
  int8_t retval = 0;
  uint16_t offset = (command_arg_ptr -> uint16_arg);
  if ( offset > AMPLIFIER_MAX_VOLUME ) {
    command_nack(NACK_ARGUMENT_OUT_OF_RANGE);
    return;
  }
  retval = eeprom_write_woofer_offset( (uint8_t) offset );
  system_state.woofer_volume_offset = offset;
  if (retval == 0) {
    command_ack();
  } else {
    command_nack(NACK_COMMAND_FAILED);
  }
  return;
}

void cmd_woff_q( command_arg_t *command_arg_ptr ) {
  usart_printf( "%u%s", system_state.woofer_volume_offset, LINE_TERMINATION_CHARACTERS );
}

void cmd_toff( command_arg_t *command_arg_ptr ) {
  int8_t retval = 0;
  uint16_t offset = (command_arg_ptr -> uint16_arg);
  if ( offset > AMPLIFIER_MAX_VOLUME ) {
    command_nack(NACK_ARGUMENT_OUT_OF_RANGE);
    return;
  }
  retval = eeprom_write_tweeter_offset( (uint8_t) offset );
  system_state.tweeter_volume_offset = offset;
  if (retval == 0) {
    command_ack();
  } else {
    command_nack(NACK_COMMAND_FAILED);
  }
  return;
}

void cmd_toff_q( command_arg_t *command_arg_ptr ) {
  usart_printf( "%u%s", system_state.tweeter_volume_offset, LINE_TERMINATION_CHARACTERS );
}
