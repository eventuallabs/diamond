#ifndef SPI_H
#define SPI_H

void spi_init( void );

// Write a byte to the SPI
uint8_t spi_write( uint8_t data );

#endif
