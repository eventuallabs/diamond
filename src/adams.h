

#ifndef ADAMS_H
#define ADAMS_H

// Provides boolean data types
#include <stdbool.h>

//************************* LED bitshifts **************************//

// Front panel knob LEDs

// Most counter-clockwise LED is at position 0.  Indexes increase as
// you go clockwise from zero.  Definitions are 64-bit bitfields.
#define KNOB_LED_0 (uint64_t)(1<<7)
#define KNOB_LED_1 (uint64_t)(1<<6)
#define KNOB_LED_2 (uint64_t)(1<<5)
#define KNOB_LED_3 (uint64_t)(1<<4)
#define KNOB_LED_4 (uint64_t)(1<<3)
#define KNOB_LED_5 (uint64_t)(1<<2)
#define KNOB_LED_6 (uint64_t)(1<<1)
#define KNOB_LED_7 (uint64_t)(1<<15)
#define KNOB_LED_8 (uint64_t)(1<<14)
#define KNOB_LED_9 (uint64_t)(1<<13)

// Maximum brightness for LED breathing around the knob
#define KNOB_BREATHE_MAX 255

// Minimum breathe brightness.  Sometimes there's a big discontuity at zero.
#define KNOB_BREATHE_MIN 10

// Brightness while the knob is turning
#define KNOB_TURNING_BRIGHTNESS 255

// Volume ADC readings less than these thresholds will light all LEDs
// up to the corresponding LED
//
// The next LED in the chain will turn on when the volume reading is
// above these thresholds.

#define KNOB_LED_0_THRESHOLD 40
#define KNOB_LED_1_THRESHOLD 100
#define KNOB_LED_2_THRESHOLD 300
#define KNOB_LED_3_THRESHOLD 400
#define KNOB_LED_4_THRESHOLD 500
#define KNOB_LED_5_THRESHOLD 600
#define KNOB_LED_6_THRESHOLD 700
#define KNOB_LED_7_THRESHOLD 800
#define KNOB_LED_8_THRESHOLD 900



//******************** State of the front panel ********************//

typedef struct adams_status_struct {

  // Current LED value
  uint64_t fp_leds_value;

  // Are the front panel LEDs getting brighter during the breathe?
  bool getting_brighter;


} adams_state_t;



//********************** Function prototypes ***********************//

// Set up Adams
int8_t adams_init(void);

// Set the chip-select state (1 or 0) for communication with adams
int8_t adams_set_cs(uint8_t data);

// Set the not output-enable state (1 or 0) for communication with adams
int8_t adams_set_noe(uint8_t data);

// Set the front panel LED values by calling adams_write().  This
// function will also write the LED values to the status structure.
int8_t adams_set_leds( uint64_t data );

// Set LEDs up to the volume knob position
//
// Arguments:
//   volume_counts -- ADC reading from the volume knob
int8_t adams_track_volume( uint16_t volume_counts );

// Write data to the front panel, discarding the return
int8_t adams_write( uint64_t data );

// Set the front panel brightness -- 0 to 255
//
// Arguments:
//
//   brightness -- 0 to 255, higher numbers are brighter
int8_t adams_set_brightness( uint8_t brightness );

// Get the current brightness setting
uint8_t adams_get_brightness( void );

// Turn all knob LEDs on
int8_t adams_set_all_knob_leds( void );

// Increment the front panel LED brightness as part of front panel
// breathing.  This function should be called from a scheduled task in
// the system module.
void adams_increment_breathe( void );

#endif
