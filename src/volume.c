#include <stdio.h>

// Device-specific port definitions.  Also provides special
// bit-manipulations functions like bit_is_clear and
// loop_until_bit_is_set.
#include <avr/io.h>

// Library functions and macros for AVR interrupts
#include <avr/interrupt.h>

// Provides macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

// Bit manipulation for AVR
#include "avr035.h"

// Provides logger_msg and logger_msg_p for log messages tagged with a
// system and severity.
#include "logger.h"

// Functions for maintaining a simple schedule
#include "OS.h"

#include "volume.h"

// Place to store summed volume knob readings before division.
uint32_t volume_sum = 0;

void volume_init() {
  logger_msg_p("volume",log_level_INFO, PSTR("Initializing volume\r\n"));

  // Set the reference to use Vcc
  SETBIT( ADMUX, REFS0 );

  // Set mux to ADC6
  //
  // Zero the bitfield
  ADMUX &= ~( _BV(MUX3) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0) );
  // Setting 0x6 corresponds to ADC6
  ADMUX |= _BV(MUX2) | _BV(MUX1);

  // The ADC requires a conversion clock (fsar) between 50 and 200kHz
  // for 10-bit resolution.  The clock can be set as high as fosc/2
  // for lower resolution.  fsar = fosc / prescale.
  //
  // Complete conversions take 13 cycles (Tcnv).  The maximum sample
  // rate is then about 1/Tcnv (fs)

  // |------------+----------+------------+-----------+----------|
  // | fosc (MHz) | prescale | fsar (kHz) | Tcnv (us) | fs (kHz) |
  // |------------+----------+------------+-----------+----------|
  // |          8 |       64 |        125 |       104 |      9.6 |
  // |         16 |      128 |        125 |       104 |      9.6 |
  // |------------+----------+------------+-----------+----------|
  ADCSRA |= _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);

  // Enable the ADC
  ADCSRA |= _BV(ADEN);

  // By default, the ADC does not use a conversion trigger.  Writing
  // ADSC will start a single conversion.  The first conversion takes
  // extra time, so we'll make it here.
  ADCSRA |= _BV(ADSC);

  // The ADCS bit will be cleared by hardware when the conversion is
  // complete.
  loop_until_bit_is_clear(ADCSRA, ADSC);
}

uint16_t volume_read() {
  uint16_t adc_temp = 0;
  uint16_t adc_average = 0;
  volume_sum = 0;

  // Enable the ADC (again).  The mux position has already been set in
  // volume_init().
  ADCSRA |= _BV(ADEN);

  // At a read rate of 10kHz, I can read 10 times in 1ms.  Keep the
  // number of reads below 10 so the volume read task takes below 1ms.
  for ( uint8_t count = 0; count < 4; count++ ) {

    // Start a single conversion
    ADCSRA |= _BV(ADSC);

    // Wait for conversion to finish
    loop_until_bit_is_clear(ADCSRA, ADSC);

    // Read the lower 8 bits, then add the upper 8 bits
    adc_temp = ADCL;
    adc_temp += (ADCH << 8);
    volume_sum += adc_temp;
  }
  adc_average = (uint16_t)(volume_sum >> 2);
  logger_msg_p("volume", log_level_INFO, PSTR("Volume ADC at %i\r\n"),adc_average);
  return adc_average;
}
