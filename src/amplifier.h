// Amplifier functions

#ifndef AMPLIFIER_H
#define AMPLIFIER_H


// |-----------+--------------+-----------+-------------+-----------+-------------+-------------|
// | Amplifier | Click socket | ADDR1 pin | ADDR1 value | ADDR2 pin | ADDR2 value | I2C address |
// |-----------+--------------+-----------+-------------+-----------+-------------+-------------|
// | Tweeter   |            1 | PORTE3    |           1 | PORTB4    |           0 |        0x49 |
// | Woofer    |            2 | PORTE4    |           0 | PORTB5    |           1 |        0x4a |
// |-----------+--------------+-----------+-------------+-----------+-------------+-------------|

#define AMPLIFIER_TWEETER_I2C_ADDRESS 0x49
#define AMPLIFIER_WOOFER_I2C_ADDRESS 0x4a

// The maximum volume setting for the MAX9744 is 63
#define AMPLIFIER_MAX_VOLUME 63

// Amplifiers
typedef enum amplifier_module {
			       AMPLIFIER_MODULE_WOOFER,
			       AMPLIFIER_MODULE_TWEETER
} amplifier_module_t;


// Set initial values
int8_t amplifier_init(void);



// Shutdown an amplifier
//
// Arguments:
//   amplifier -- AMPLIFIER_MODULE_WOOFER or AMPLIFIER_MODULE_TWEETER
//   shutdown -- True for shutdown, False to come out of shutdown
int8_t amplifier_shutdown( amplifier_module_t amplifier, bool shutdown);

// Mute an amplifier
//
// Arguments:
//   amplifier -- AMPLIFIER_MODULE_WOOFER or AMPLIFIER_MODULE_TWEETER
//   mute -- True for mute, False to come out of mute
int8_t amplifier_mute( amplifier_module_t amplifier, bool mute);

// Set woofer amplfier volume
//
// Arguments:
//   level -- 1-63
int8_t amplifier_set_woofer_volume(uint8_t level);

// Set tweeter amplifier volume
//
// Arguments:
//   level -- 1-63
int8_t amplifier_set_tweeter_volume(uint8_t level);

#endif
