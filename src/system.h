
// The system module provides miscellaneous structures and
// functions to support the system.

#ifndef SYSTEM_H
#define SYSTEM_H

// Provides the definition of command_t -- the variable type
// containing the attributes of each remote command.
#include "command.h"

// Provides boolean data types
#include <stdbool.h>

// System states
typedef enum system_state_enum {
				system_state_INIT,
				system_state_CCW,
				system_state_TURNING,
				system_state_UNMUTED,
} system_state_value_t;

// The system will look at the volume knob position with this period (ms)
#define VOLUME_KNOB_CHECK_INTERVAL_MS 100

// Overall system state structure
typedef struct system_status_struct {
  // System serial number
  uint16_t sernum;

  // System state -- one of an enumerated type
  system_state_value_t state_enum;

  // Is the woofer amplifier connected?
  bool woofer_amplifier_present;

  // Is the tweeter amplifier connected?
  bool tweeter_amplifier_present;

  // Are the tweeters muted?
  bool tweeters_muted;

  // Is the woofer muted?
  bool woofer_muted;

  // Volume knob position
  uint16_t volume_knob_position;

  // Breathe LED task number
  uint8_t breathe_leds_task_number;

  // Exit turning task number
  uint8_t exit_turning_task_number;

  // Woofer volume offset
  uint8_t woofer_volume_offset;

  // Tweeter volume offset
  uint8_t tweeter_volume_offset;

} system_state_t;

// The system state structure variable will have global scope, and
// will be defined in system.c
extern system_state_t *system_state_ptr;

// Enter ccw state
//
// Volume knob is all the way down
// Mute amplifiers, breathe lowest LED
void system_enter_ccw( void );

// Enter turning state
//
// Volume LEDs track the volume setting
void system_enter_turning( void );

void system_enter_unmuted( void );

// Print identity string
void system_print_identity(void);

// Initialize the system state structure.  This populates the
// structure with non-volatile values from eeprom.
void system_init( void );

// Start the breathing task
int8_t system_start_breathing( void );

// Stop the breathing task and reset the LED brightness
int8_t system_stop_breathing( void );

//***************** Tasks called by the scheduler ******************//

// Check the volume knob and set amplifier volume
//
// Must be void type to work with the scheduler
void system_volume_task( void );

// Let everyone know the system is still alive
void system_heartbeat_task( void );

// Breathe the front panel LEDs
void system_breathe_task( void );

void system_exit_turning_task( void );

// Report the free stack memory
void system_report_stack_free( void );

//******* Set and get members of the system_state structure ********//

// Try to set the system state.  Return the actual system state, which
// may not be what you asked for.
system_state_value_t set_system_state(system_state_value_t requested_state);

// Set the woofer amplifier connected state
int8_t system_state_set_woofer_amplifier_present(bool present);

// Get the woofer amplifier connected state
bool system_state_get_woofer_amplifier_present(void);

// Set the tweeter amplifier connected state
int8_t system_state_set_tweeter_amplifier_present(bool present);

// Get the tweeter amplifier connected state
bool system_state_get_tweeter_amplifier_present(void);

// Mute/unmute the tweeters and set the tweeters_muted state
//
// Arguments:
//   muted -- True to disable (mute) tweeters, false to enable (unmute)
int8_t system_set_tweeters_muted( bool muted );

// Get the muted state of the tweeters.  Returns true for muted, false otherwise.
bool system_get_tweeters_muted( void );

// Mute/unmute the woofer and set the woofer_muted state
//
// Arguments:
//   muted -- True to disable (mute) woofer, false to enable (unmute)
int8_t system_set_woofer_muted( bool muted );

// Get the muted state of the woofer.  Returns true for muted, false otherwise.
bool system_get_woofer_muted( void );

//************************ Remote commands *************************//

// Function called by the *idn? command
void cmd_idn_q( command_arg_t *command_arg_ptr );

// Function called by the *rst command
void cmd_rst( command_arg_t *command_arg_ptr );

// Function called by the opstate? command
void cmd_opstate_q( command_arg_t *command_arg_ptr );

// Function called by the wmute command
void cmd_wmute( command_arg_t *command_arg_ptr );

// Function called by the wmute? command
//
// Returns "muted" if the woofer is muted, "unmuted" otherwise
void cmd_wmute_q( command_arg_t *command_arg_ptr );

// Function called by the tmute command
void cmd_tmute( command_arg_t *command_arg_ptr );

// Function called by the tmute? command
//
// Returns "muted" if the tweeters are muted, "unmuted" otherwise
void cmd_tmute_q( command_arg_t *command_arg_ptr );

// Write the woofer volume offset (woff)
void cmd_woff( command_arg_t *command_arg_ptr );

// Query the woofer volume offset (woff?)
void cmd_woff_q( command_arg_t *command_arg_ptr );

// Write the tweeter volume offset (toff)
void cmd_toff( command_arg_t *command_arg_ptr );

// Query the tweeter volume offset (toff?)
void cmd_toff_q( command_arg_t *command_arg_ptr );



#endif // End the include guard
