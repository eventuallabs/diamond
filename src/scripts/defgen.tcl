######################## Global configuration ########################

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name defgen

# This software's version.  Anything set here will be clobbered by the
# makefile when starpacks are built.
set revcode 1.0

set root_directory [file dirname $argv0]

######################## Command line parsing ########################

package require cmdline
set usage "usage: [file tail $argv0] \[options]"

set options {
    {n.arg "DEFGEN" "Name to use for prefix"}
    {p.arg "A" "Port letter"}
    {b.arg "1" "Bit number"}

}

try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {message optdict} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $message
    exit 1
}


proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

proc port_reg_define {} {
    global params
    set defline "#define "
    append defline "[string toupper $params(n)]_PORT_REG PORT[string toupper $params(p)]"
    return $defline
}

proc port_bit_define {} {
    global params
    set defline "#define "
    append defline "[string toupper $params(n)]_PORT_BIT P[string toupper $params(p)][string toupper $params(b)]"
    return $defline
}

proc ddr_reg_define {} {
    global params
    set defline "#define "
    append defline "[string toupper $params(n)]_DDR_REG DDR[string toupper $params(p)]"
    return $defline
}

proc ddr_bit_define {} {
    global params
    set defline "#define "
    append defline "[string toupper $params(n)]_DDR_BIT DD[string toupper $params(p)][string toupper $params(b)]"
    return $defline
}


########################## Main entry point ##########################

set port_reg_define_line [port_reg_define]
set port_bit_define_line [port_bit_define]
set ddr_reg_define_line [ddr_reg_define]
set ddr_bit_define_line [ddr_bit_define]

puts $port_reg_define_line
puts $port_bit_define_line
puts $ddr_reg_define_line
puts $ddr_bit_define_line
