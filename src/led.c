
#include <stdio.h>

// Device-specific port definitions.  Also provides special
// bit-manipulations functions like bit_is_clear and
// loop_until_bit_is_set.
#include <avr/io.h>

#include "avr035.h"

// Functions for maintaining a simple schedule
#include "OS.h"

#include "led.h"

led_state_t led_state;

// ----------------------- Functions ----------------------------------

void led_init(void) {

  // Make the debug LED into an output
  SETBIT( LED_DEBUG_DDR_REG, LED_DEBUG_DDR_BIT );
  led_set_debug_led( false );

  // Create a task to allow pulsing this LED
  OS_TaskCreate(&led_debug_led_off_task, LED_DEBUG_PULSE_MS, SUSPENDED);
  led_state.debug_led_off_task_number = OS_get_task_number(&led_debug_led_off_task);
}

void led_debug_led_off_task(void) {
  led_set_debug_led( false );
  OS_SetTaskState(led_state.debug_led_off_task_number, SUSPENDED);
}

void led_pulse_debug_led(void) {
  led_set_debug_led( true );
  OS_SetTaskState(led_state.debug_led_off_task_number, BLOCKED);
}

void led_set_debug_led( bool setting ) {
  if (setting) {
    // Turn LED on
    SETBIT( LED_DEBUG_PORT_REG, LED_DEBUG_PORT_BIT );
  } else {
    CLEARBIT( LED_DEBUG_PORT_REG, LED_DEBUG_PORT_BIT );
  }
  return;
}
