
// Simple scheduler module originally taken from Ferenc Nemeth
// https://github.com/ferenc-nemeth/

// This implementation requires a 1 ms metronome tick.

#ifndef OS_H_
#define OS_H_

#include <avr/io.h>
#include <stddef.h>

// See macros for integer constants:
// https://www.nongnu.org/avr-libc/user-manual/group__avr__stdint.html

// Maximum number of tasks that can be registered
#define OS_MAX_TASK_NUM UINT8_C(100)

// Maximum task period (ms)
//
// Note that you have to change the type of time_burst_ms below if you
// want this to be larger than 16 bits
#define OS_MAX_TIME_MS UINT32_C(65000)

// Minimum task period (ms)
#define OS_MIN_TIME_MS UINT8_C(1)

typedef void (*fncPtr)(void);           /**< Function pointer for registering tasks. */

// Task states
typedef enum {
	      // In the BLOCKED state the task waits for the timer to
	      // put it into READY state.
	      BLOCKED = 0,
	      // In the READY state the task is ready to be called and
	      // executed in the main function.
	      READY,
	      // In the SUSPENDED state the task is ignored by the
	      // timer and executer.
	      SUSPENDED                           
} OS_state;

/**
 * Variables of the tasks.
 */
typedef struct
{
  // Task that gets called periodically
  fncPtr function;

  // Time between tasks (ms)
  uint32_t time_burst_ms;

  // Time since the task was called
  uint32_t time_count_ms;

  // The state of the task
  OS_state    state;
} OS_struct;

/**
 * Feedback and error handling for the task creation.
 */
typedef enum
{
    OK = 0,                             /**< OK:    Everything went as expected. */
    NOK_NULL_PTR,                       /**< ERROR: Null pointer as a task. */
    NOK_TIME_LIMIT,                     /**< ERROR: The time period is more or less, than the limits. */
    NOK_CNT_LIMIT,                      /**< ERROR: Something horrible happened, time to panic! */
    NOK_UNKNOWN
} OS_feedback;

/**
 * Functions.
 */

// Get the number of registered tasks
uint8_t OS_get_total_tasks(void);

// Find the task number of a task with the given function pointer
int8_t OS_get_task_number(fncPtr);

OS_feedback OS_TaskCreate(fncPtr task, uint16_t default_time_burst, OS_state default_state);
void OS_TaskTimer(void);
void OS_TaskExecution(void);
OS_state OS_GetTaskState(uint8_t task_number);

// Get the ms between task executions
uint32_t OS_GetTaskBurstTime(uint8_t task_number);

// Get the ms remaining before the task is executed
uint32_t OS_GetTaskCntTime(uint8_t task_number);
void OS_SetTaskState(uint8_t task_number, OS_state new_state);

// Set the ms between task executions
void OS_SetTaskBurstTime(uint8_t task_number, uint32_t new_time_burst);

// Set the task's counter value (counters count up)
void OS_SetTaskCntTime(uint8_t task_number, uint32_t new_time_cnt);

#endif /* OS_H_ */
