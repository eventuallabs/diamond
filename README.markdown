# Diamond #

Firmware for the EVL-510 desktop speaker.

![Bullhead hero](img/bullhead_hero_328x227.jpg)

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Diamond](#diamond)
    - [Hardware overview](#hardware-overview)
    - [Prerequisites](#prerequisites)
        - [avr-gcc toolchain](#avr-gcc-toolchain)
            - [Windows](#windows)
            - [Linux (Ubuntu)](#linux-ubuntu)
        - [Pololu USB programmer](#pololu-usb-programmer)
        - [Findcoms](#findcoms)
        - [I2Clint (optional)](#i2clint-optional)
        - [Arduino Nano](#arduino-nano)
    - [Pin and timer allocation](#pin-and-timer-allocation)
    - [Building images](#building-images)
        - [Compiler flags](#compiler-flags)
            - [REVCODE](#revcode)
            - [LOG_LEVEL](#log_level)
    - [Bootloader](#bootloader)
        - [Writing the bootloader](#writing-the-bootloader)
        - [Updating the application hex](#updating-the-application-hex)
    - [Volume control](#volume-control)
    - [ASCII command parser](#ascii-command-parser)
    - [Serial connection details](#serial-connection-details)
        - [Command interface](#command-interface)
        - [Debug interface](#debug-interface)
    - [Command reference](#command-reference)
        - [System commands](#system-commands)
            - [\*IDN?](#idn)
                - [Parameter](#parameter)
                - [Usage example](#usage-example)
        - [Volume offsets](#volume-offsets)
            - [WOFF(?) n](#woff-n)
                - [Parameter (n)](#parameter-n)
                - [Usage example](#usage-example-1)
    - [A very simple scheduler](#a-very-simple-scheduler)

<!-- markdown-toc end -->

## Hardware overview ##

I designed Diamond to run the EVL-510 hardware illustrated below. This
is a desktop speaker with two tweeters and a center woofer. All
speakers are driven by `MAX7944` amplifiers, which get their volume
settings over an I2C interface. Analog input comes in through a front
panel 3.5mm jack.  An analog crossover sends low frequencies to the
woofer, and high frequencies to the tweeters.

A front panel knob turns a potentiometer sampled by an ADC. These ADC
readings set amplifier volumes and change the front panel LED patterns.


![Hardware overview](img/hardware_overview.png)

## Prerequisites ##

You don't need all of the EVL-510 hardware to work with Diamond, but
you will need some tools and the Arduino Nano hardware described in
the next few sections.

### avr-gcc toolchain ###

  * **avr-gcc** compiler
  * **avr-libc** libraries
  * **avrdude** device programmer
  * **GNU make** for build automation

#### Windows ####

[Zak's Electronics
Blog](https://blog.zakkemble.net/category/microcontroller/avr/) is a
great place to find pre-built AVR toolchains for Windows.  These also
include GNU Make.

#### Linux (Ubuntu) ####

There are many ways to get these tools for Linux.  With Ubuntu, you can
```
sudo apt install gcc-avr binutils-avr gdb-avr avr-libc avrdude
```
to get recent-enough versions.  This is not a new microcontroller.

### Pololu USB programmer ###

Pololu makes a very nice [AVR
programmer](https://www.pololu.com/product/3172) that also works to
capture output from the debug UART.  This project's makefile uses this
programmer by default to write the bootloader, fuses, and application
hex files.

![Pololu programmer](img/pololu_usb_avr_programmer_339x320.png)

### Findcoms ###

The source makefile uses [findcoms](https://gitlab.com/eventuallabs/findcoms)
to detect the Pololu programmer and the FTDI device on the Arduino
Nano.  This is a Tcl script, so you'll need Tcl.  Clone the
**findcoms** repository into the **diamond** source tree at `src/scripts/findcoms`.

### I2Clint (optional) ###

The documentation makefile uses
[I2Clint](https://gitlab.com/eventuallabs/i2clint) to list I2C
addresses in the system. This isn't a big deal for this project, since
there are only two addresses.  I2Clint is more useful if you need to
keep track of many I2C devices on many boards.

![i2clint](img/i2clint.png)

### Arduino Nano ###

Diamond uses the [Arduino
Nano](https://www.seeedstudio.com/Arduino-Nano-v3-p-1928.html)
hardware, but not the rest of the Arduino framework.  You'll also need
a 2x6 ribbon cable to program fuses and the bootloader.

![Arduino nano](img/arduino_nano_412x320.png)

## Pin and timer allocation ##

Look at the [Pin and Timer Usage](doc/pin_usage.ods) spreadsheet for
the pin and timer (separate tab) allocations.

![Pin spreadsheet](img/pin_usage_grab.png)

## Building images ##

From the `src` directory,

```
make hex
```

...will build the image.  I pass a few definitions, described below,
to the compiler to manage things like log level and version code.

### Compiler flags ###

#### REVCODE ####

This is the firmware revision.  This needs to be set in the makefile
so that the released image can be named correctly.

#### LOG_LEVEL ####

Firmware images behave differently with different levels of logging
turned on, so the image name really should contain the log level.  See
the makefile for the recognized settings, like `debug` and `warning`.
Keep in mind that the logger is configured both by subsystem (like
`eeprom` and `amplifier`) and by severity (like `debug` or `error`).

## Bootloader ##

The bootloader image is built with [Optiboot](https://github.com/Optiboot/optiboot) and copied to `bootloader/optiboot_atmega328.hex`. You can build this yourself with

```
cd optiboot/bootloaders/optiboot
make AVR FREQ=16000000L BAUD RATE=38400 SINGLESPEED=1 UART=0 atmega328
```

...after cloning the Optiboot repository.  We can check the bootloader's size with

```
diamond/bootloader $ avr-size optiboot_atmega328.hex
   text	   data	    bss	    dec	    hex	filename
      0	    476	      0	    476	    1dc	optiboot_atmega328.hex
```

...where dec is the size of the bootloader in bytes formatted as a
decimal number. The boot sizes in the AVR datasheet are in words,
which are two bytes large.  We need to make sure the `BOOTSZ0` and
`BOOTSZ1` fuses are set correctly for the bootloader's size. The
ATmega328p's minimum boot size setting is 256 words = 512 bytes. This
setting corresponds to `BOOTSZ0 = 1` and `BOOTSZ1 = 1`, where a 1
means that the fuse is "unprogrammed."  The fuse values written with

```
diamond/src $ make fusewrite
```

in the Diamond makefile reflect these settings.

### Writing the bootloader ###

Use

```
diamond/src $ make blflash
```

in the Diamond makefile to flash the bootloader using the Pololu programmer.

### Updating the application hex ###

Once the fuses and bootloader are written, you can upload your new application hex files with

```
diamond/src $ make appload
```

...in the Diamond makefile. This requires the **Findcoms** software
described earlier to locate the Arduino Nano's FTDI device node.

## Volume control ##

The overall system state machine is driven by the volume control
knob. As shown below, `system_volume_task` runs periodically
(currently every 100ms) to decide if the system state needs to be
changed. Most of the system's activity is driven by entry and exit
functions for the various system states.  See `system.c` to see the
`case` statements that handle these transitions.

![volume knob states](img/volume_knob_states.png)

## ASCII command parser ##

Diamond is able to receive, recognize, and process short ASCII
commands with a single argument.  As shown below, command processing
begins with loading received characters into a buffer.

![incoming character flow](img/incoming_character_flow.png)

Characters are loaded into buffers with an Interrupt Service Routine
(ISR), but commands are handled in the main loop.  Commands are not
buffered, so you can get into situations where the command handler
can't keep up.  The system will then return various error codes
described in the remote command reference.

Assembled commands are handled according to the flow shown below.
There are some things here that are pretty crude, like the argument
range check.  That simply compares the number of argument characters
received against a maximum number.  There are lots of ways to make
this better.

![incoming command flow](img/incoming_command_flow.png)

## Serial connection details ##

### Command interface ###

The command interface is via USB, but the hardware will enumerate as a
virtual serial port.  The port will need to be configured with these
settings:

| Baud   | Data bits | Stop bits | Parity checking |
|--------|-----------|-----------|-----------------|
| 38400  | 8         | 1         | None            |

### Debug interface ###

The debug interface is a bit-banged UART output illustrated below.  I like to connect this to the same Pololu USB transceiver I use to write the bootloader.  This is a combination ISP programmer and USB / UART interface.

![debug output wiring](img/debug_output_wiring_blog.png)

The debug serial port needs to be configured with these settings:

| Baud   | Data bits | Stop bits | Parity checking |
|--------|-----------|-----------|-----------------|
| 19200  | 8         | 1         | None            |

## Command reference ##

Commands are not case-sensitive -- the command `*IDN?` is the same as
`*idn?`.

Commands always return something.  If the command doesn't have an
explicit return value or string, it will return 0 (ACK).  Unrecognized
commands or commands causing other problems will return one of the
not-acknowledged (NACK) codes shown below.

| NACK code | Condition                |
|-----------|--------------------------|
| -1        | Command not recognized   |
| -2        | System is busy           |
| -3        | Command execution failed |
| -4        | Buffer overflow          |
| -5        | Argument out of range    |


### System commands ###

#### \*IDN? ####

Returns an identification string containing four comma-separated fields:

1. Manufacturer name
2. Firmware project name
3. Hardware serial number
4. Firmware revision code

##### Parameter #####

None

##### Usage example #####

```
*idn?
Eventual Labs,Diamond,SN2,1.0.3
```

### Volume offsets ###


#### WOFF(?) n ####

Set or query the woofer volume offset. This number is added to the
volume read from the front panel knob.

##### Parameter (n) #####

Integers 0-63

##### Usage example #####

```
woff 3
0
woff?
3
```

## A very simple scheduler ##

I found [a very simple
scheduler](https://github.com/ferenc-nemeth/avr-simple-scheduler) for
AVRs and adapted it slightly for Diamond. The scheduler relies on
`OS_TaskTimer()` being called often enough for tasks to execute in a
predictable way.  My `metronome` module configures `timer1` to fire an
interrupt every 1ms, calling `OS_TaskTimer()` in the Interrupt Service
Routine (ISR). This timing allows the time parameter in
`OS_TaskCreate()` to be the duration between task executions in
integer milliseconds.

Tasks in the execution queue are executed when the main loop gets to
`OS_TaskExecution()`.  This function needs to be called at least as
often as the scheduler runs, but it can be delayed by the remote
interface.  My design goals allow for sending remote commands to delay
some scheduled tasks, since these commands won't be sent continuously. 
