namespace eval diamond {
    # Procedures and variables unique to the diamond project

    # The firmware version to check during testing
    variable version 1.0.3

    # The woofer volume offset
    variable woofer_volume_offset_ok false
    variable woofer_volume_offset 0

    
    
    proc init {channel} {
	# Send a carriage return to reset the command state
	puts -nonewline $channel "\r"
	after 100
    }

    proc sendcmd {channel data} {
	global state
	global command_dict
	global log
	set command_issue_time_ms [clock milliseconds]
	chan event $channel readable {set response "OK"}
	after 5000 {set response "timeout"}
	set payload "${data}\r"
	puts -nonewline $channel $payload
	vwait response
	set channel_readable_time_ms [clock milliseconds]
	set command_response_latency_ms [expr $channel_readable_time_ms - $command_issue_time_ms]
	dict set command_dict $data latency_ms $command_response_latency_ms
	${log}::debug "Command $data returned in $command_response_latency_ms ms"
	# Wait for the return
	after 200
    }

    proc readline {channel} {
	set data [chan gets $channel]
	return $data
    }
}
