# --------------------- Global configuration --------------------------

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name diamond_test

# The base filename for the execution log.  The actual filename will add
# a number after this to make a unique logfile name.
set execution_logbase "diamond_test"

# Set the log level.  Known values are:
# debug
# info
# notice
# warn
# error
# critical
# alert
# emergency
set loglevel debug

set root_directory [file dirname $argv0]

# ---------------------- Command line parsing -------------------------
package require cmdline
set usage "usage: [file tail $argv0] \[options]"

set options {
    {f.arg "all" "Single test file to run"}
    {o.arg "lastrun.log" "Output file"}
}

try {
    array set params [cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $msg
    exit 1
}

# Create a dictionary to keep track of global state
# State variables:
#   program_name --  Name of this program (for naming the window)
#   program_version -- Version of this program
#   thisos  -- Name of the os this program is running on
#   exelog -- The execution log filename
set state [dict create \
	       program_name $program_name \
	       thisos $tcl_platform(os) \
	       exelog none
	  ]

# --------------------- Tools for code modules ------------------------
source [file join $root_directory module_tools.tcl]

# Math tools
source [file join $root_directory jpmath.tcl]

#----------------------------- Set up logger --------------------------

# The logging system will use the console text widget for visual
# logging.

package require logger
source loggerconf.tcl
${log}::info [modinfo logger]

proc source_script {file args} {
    # Execute a tcl script by sourcing it.  Note that this will
    # clobber your existing argument list.
    set argv $::argv
    set argc $::argc
    set ::argv $args
    set ::argc [llength $args]
    set code [catch {uplevel [list source $file]} return]
    set ::argv $argv
    set ::argc $argc
    return -code $code $return
}

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

# Testing the logger

puts "Current loglevel is: [${log}::currentloglevel]"
${log}::info "Trying to log to [dict get $state exelog]"
${log}::info "Known log levels: [logger::levels]"
${log}::info "Known services: [logger::services]"
${log}::debug "Debug message"
${log}::info "Info message"
${log}::warn "Warn message"

############################## tcltest ###############################
package require tcltest
tcltest::configure -singleproc true

if {$params(f) != "all"} {
    # Only run tests in the file specified from the command line
    tcltest::configure -file "$params(f)"
}

# Skip pressure commands
# tcltest::configure -notfile "pressure.test"

# Only run the channel locations test.  Comment this out to run all
# tests.
# tcltest::configure -file "channel_locations.test"

# Set verbosity to print output when a test passes
tcltest::configure -verbose {body pass start error}

# Delete the log file
file delete -force $params(o)
tcltest::configure -outfile $params(o)
${log}::info "Writing output to $params(o)"

source connection.tcl
source diamond.tcl
${log}::debug "Potential connection nodes: [connection::get_potential_aliases]"
foreach alias [connection::get_potential_aliases] {
    set channel [connection::is_available $alias "38400,n,8,1"]
    if { ![string equal $channel false] } {
	# This is a viable connection alias, and it's now going
	# through a reset.  The USB connection asserts DTR after the
	# channel is configured, resetting the part.  We have to wait
	# for the bootloader to time out and for rebooting to happen
	# before we can send commands.
	${log}::debug "Alias $alias can be configured"
	set connection_start_time_ms [clock milliseconds]

	# Wait for unsolicited data to come through, or timeout if there's no data
	set data [connection::wait_for_data $channel 5 5000]

	set connection_finish_time_ms [clock milliseconds]
	# Clear out the Rx queue
	append data [connection::read_until_timeout $channel 100]
	set boot_time_ms [expr $connection_finish_time_ms - $connection_start_time_ms]
	if {[string length $data] == 0} {
	    ${log}::debug "No response from $alias in $boot_time_ms ms"
	    # continue
	} else {
	    ${log}::debug "$alias responded with: [string trim $data] in $boot_time_ms ms"    
	}	

	dict set state channel $channel
	dict set state alias $alias
	# Ask for identity
	diamond::sendcmd $channel "*idn?"
	# Read the response
	try {
	    set data [diamond::readline $channel]
	    ${log}::debug "Response to *idn? was $data"
	    if {[string last "Eventual" $data] >= 0} {
		# We found the string we wanted to find in the response
		${log}::info "Successful connection to Diamond at $alias"
		break
	    } else {
		dict set state channel "none"
		dict set state alias "none"
	    }
	} trap {} {message optdict} {
	    # We couldn't read from the channel -- this is definitely
	    # not a device we want to talk to.
	    dict set state channel "none"
	    dict set state alias "none"
	}
    }
}

if [string equal [dict get $state channel] "none"] {
    ${log}::error "Could not talk to diamond."
    exit
}

# Create a dictionary to keep track of commands tested and their execution times
set command_dict [dict create]

tcltest::runAllTests

set column_width 20
dict set table_dict command width $column_width
dict set table_dict command description "Command"

dict set table_dict response_time_ms width $column_width
dict set table_dict response_time_ms description "Response time (ms)"


foreach column [dict keys $table_dict] {
    append format_string "%-*s "
    lappend header_list [dict get $table_dict $column width]
    lappend header_list [dict get $table_dict $column description]
}
set format_string [string trim $format_string]
set header [format $format_string {*}$header_list]

puts ""
puts $header

puts [dashline [string length $header]]

foreach command [dict keys $command_dict] {
    set row_list [list]
    lappend row_list $column_width
    lappend row_list $command
    lappend row_list $column_width
    lappend row_list [dict get $command_dict $command latency_ms]
    puts [format $format_string {*}$row_list]
}
