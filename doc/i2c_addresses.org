* Diamond I2C addresses
  :PROPERTIES:
  :COLUMNS: %25ITEM %i2c_chain(chain) %device %pcb_name(PCB)
  :END:
  - This file can be scanned with i2clint


** Woofer amplifier
   :PROPERTIES:
   :device:   MAX9744
   :manufacturer: Maxim
   :pcb_pn:   13-17
   :pcb_name: Bughouse
   :pcb_revision: a
   :i2c_chain: M 0x4a
   :END:
   - There are two address pins, each of which can be high or
     low...but both can not be low at the same time.  
|----+----+---------|
| A1 | A0 | Address |
|----+----+---------|
|  0 |  0 |    0x48 |
|  0 |  1 |    0x49 |
|  1 |  0 |    0x4a |
|  1 |  1 |    0x4b |
|----+----+---------|

** Tweeter amplifier
   :PROPERTIES:
   :device:   MAX9744
   :manufacturer: Maxim
   :pcb_pn:   13-17
   :pcb_name: Bughouse
   :pcb_revision: a
   :i2c_chain: M 0x49
   :END:
   - There are two address pins, each of which can be high or
     low...but both can not be low at the same time.  
|----+----+---------|
| A1 | A0 | Address |
|----+----+---------|
|  0 |  0 |    0x48 |
|  0 |  1 |    0x49 |
|  1 |  0 |    0x4a |
|  1 |  1 |    0x4b |
|----+----+---------|
